'use strict';

const fc = require('fast-check');

const DATADIR = process.env.DATADIR;
const outputSql = require( '../../../output-sql.js' );
const fs = require( 'fs' );

fc.configureGlobal({ numRuns: 10000 });

var as_strat = fc.record(
	{
		organization: fc.string(),
		number: fc.nat()
	},
	{ requiredKeys: [] }
);

var concentration_strat = fc.record(
	{
		city: fc.string(),
		country: fc.string(),
		density: fc.nat(),
		geohash: fc.string(),
		skew: fc.float( { min: 0, max: 1 } ),
		state: fc.string()
	},
	{ requiredKeys: [] }
);

var client_strat = fc.record(
	{
		behaviors: fc.array(fc.string()),
		concentration: concentration_strat,
		count: fc.nat(),
		countries: fc.nat(),
		proxies: fc.array(fc.string()),
		spread: fc.nat(),
		types: fc.subarray(['DESKTOP', 'HEADLESS', 'IOT', 'MOBILE'])
	},
	{ requiredKeys: [] }
);

var location_strat = fc.dictionary(fc.constantFrom('city', 'country', 'state'), fc.string());

var tunnels_strat = fc.array(fc.record(
	{
		type: fc.constantFrom("VPN", "PROXY"),
		operator: fc.string(),
		anonymous: fc.boolean(),
		entries: fc.array(fc.string()),
		exits: fc.array(fc.string())
	},
	{ requiredKeys: [ 'type' ] }
));

var spur = fc.record(
	{
		as: as_strat,
		client: client_strat,
		infrastructure: fc.subarray(['DATACENTER', 'MOBILE', 'SATELLITE', 'IN_FLIGHT_WIFI', 'GOOGLE']),
		location: location_strat,
		organization: fc.string(),
		risks: fc.subarray(['CALLBACK_PROXY', 'GEO_MISMATCH', 'LOGIN_BRUTEFORCE', 'TUNNEL', 'WEB_SCRAPING']),
		services: fc.array(fc.string()),
		tunnels: tunnels_strat
	},
	{ requiredKeys: [] }
);

describe( 'output-sql', () => {
	it( 'should output the correct SQL file in diff mode', () => {
		fc.assert(
			fc.property( spur, spur, function( oldLine, newLine ) {
				// Remove existing files to ensure we compare the output of this test run.
				fs.rmSync( `${DATADIR}/remove.sql`, {
					force: true
				} );
				fs.rmSync( `${DATADIR}/update.sql`, {
					force: true
				} );
				fs.rmSync( `${DATADIR}/insert.sql`, {
					force: true
				} );

				oldLine.ip = '1.2.3.4';
				newLine.ip = '1.2.3.4';
				var testData = JSON.stringify( oldLine ) + "@@@@@<\n" + JSON.stringify( newLine ) + "@@@@@>";

				// output-sql will modify the input file, so test on a disposable copy
				const inputFileDisposable = '/tmp/20000101_20000102_fake.unique.sorted.json.copy';
				fs.writeFileSync(
					inputFileDisposable,
					testData
				);
				outputSql( inputFileDisposable, 'diff' );
				return true;
			} )
		);
	} );
} );
