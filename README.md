# ipmasking testing

## Testing ipoid feed update routine

Here is a suggestion for testing ipoids daily update. It will:
* Create some initial data and import it into ipoid
* Modify that data (using `random_spur_data_updater.py`) and import the updated data into ipoid
* Each time new or updated data is imported to ipoid, check that what is in the database matches the JSON feed (using `ipoid_data_validation_2.py`)

1. Download `random_spur_data_updater.py` and `ipoid_data_validation_2.py` from this repository and put them in your local ipoid repository
2. `docker compose up -d`
3. `docker compose exec web node -e "require('./create-users.js')();"`
4. Run the below script. You could save it as a file and run it like `bash <filename> 1 5`:
```
# Full path to Spur data you want to use as test data
SOURCE_FILE=/path/to/spur.v2.feed.json.new
# Number of entries to have in your test JSON feed
NUM=100000
# Run `docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' ipoid_db_1` to find out the IP of your ipoid database
DATABASE=172.18.0.2
TEST_JSON="spur.v2.feed.new_"$1"_"$NUM".json"
TEST_GZIP=tmp/$TEST_JSON".gz"

echo "Running shuf -n $NUM $SOURCE_FILE > $TEST_JSON ..."
shuf -n $NUM $SOURCE_FILE > $TEST_JSON
echo "Running gzip -ck $TEST_JSON > $TEST_GZIP ..."
gzip -ck $TEST_JSON > $TEST_GZIP

echo "Clearing database..."
docker compose exec web node -e "require('./init-db.js')();"
echo "docker compose exec web ./diff.sh --today $TEST_GZIP"
docker compose exec web ./diff.sh --today $TEST_GZIP
echo "docker compose exec web ./import.sh 0"
docker compose exec web ./import.sh 0

echo "python3 ipoid_data_validation_2.py -f $TEST_JSON -s $DATABASE -u ipoid_ro -p password3"
python3 ipoid_data_validation_2.py -f $TEST_JSON -s $DATABASE -u ipoid_ro -p password3

for i in `seq 1 $2`
do
    MODIFIED_JSON="spur.v2.feed.new_"$1"_"$NUM"_modified_"$i".json"
    echo "python3 random_spur_data_updater.py -f $TEST_JSON -s $SOURCE_FILE -o $MODIFIED_JSON"
    python3 random_spur_data_updater.py -f $TEST_JSON -s $SOURCE_FILE -o $MODIFIED_JSON
    MODIFIED_GZIP=tmp/$MODIFIED_JSON".gz"
    echo "gzip -ck $MODIFIED_JSON > $MODIFIED_GZIP"
    gzip -ck $MODIFIED_JSON > $MODIFIED_GZIP
    echo "docker compose exec web ./diff.sh --yesterday $TEST_GZIP --today $MODIFIED_GZIP"
    docker compose exec web ./diff.sh --yesterday $TEST_GZIP --today $MODIFIED_GZIP
    docker compose exec web ./import.sh 0
    echo "python3 ipoid_data_validation_2.py -f $MODIFIED_JSON -s $DATABASE -u ipoid_ro -p password3"
    python3 ipoid_data_validation_2.py -f $MODIFIED_JSON -s $DATABASE -u ipoid_ro -p password3
    TEST_JSON=$MODIFIED_JSON
    TEST_GZIP=$MODIFIED_GZIP
done

```
