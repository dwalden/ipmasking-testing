# Copyright (C) 2023 Dominic Walden

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
# http://www.gnu.org/copyleft/gpl.html

# Example usage:
# python3 random_spur_data_updater.py -f input.json -s source.json -o output.json

import argparse
import json
import random
import time

args = argparse.ArgumentParser()
args.add_argument('-f', '--file', required=True, help="JSON feed you want updating.")
args.add_argument('-s', '--source', required=True, help="JSON feed you want to pull extra data from.")
args.add_argument('-o', '--output', required=True, help="Where you want to write the updated JSON feed.")
options = args.parse_args()

final_json = []
update = []
nothing = []
replace = []
ips = set()
start_time = time.time()
with open(options.file, "r") as fp:
    for line in fp:
        line_json = json.loads(line)
        ips.add(line_json['ip'])
        choice = random.choice(["nothing", "update", "replace"])
        if choice == "nothing":
            nothing.append(line_json['ip'])
            final_json.append(line_json)
        elif choice == "replace":
            replace.append(line_json['ip'])
        elif choice == "update":
            update.append(line_json['ip'])
print("--- Time spent processing input file: %s seconds ---" % (time.time() - start_time))

with open(options.source, "r") as source:
    start_time = time.time()
    replaced = []
    for i in range(0, len(replace)):
        rnd = random.randint(1, 100)
        j = 0
        for line in source:
            j = j + 1
            if j > rnd:
                line_json = json.loads(line)
                if line_json['ip'] not in ips:
                    replaced.append(line_json['ip'])
                    final_json.append(line_json)
                    break
    print("--- Time spent replacing entries: %s seconds ---" % (time.time() - start_time))

    start_time = time.time()
    for ip in update:
        rnd = random.randint(1, 100)
        j = 0
        for line in source:
            j = j + 1
            if j > rnd:
                line_json = json.loads(line)
                if line_json['ip'] not in ips:
                    line_json['ip'] = ip
                    final_json.append(line_json)
                    break
    print("--- Time spent updating entries: %s seconds ---" % (time.time() - start_time))

start_time = time.time()
with open(options.output, "a") as output, open("statistics_{}".format(options.output), "w") as stats:
    json.dump({"unchanged": nothing, "updated": update, "replacee": replace, "replacer": replaced}, stats)
    for json_line in final_json:
        json.dump(json_line, output, separators=(',', ':'))
        output.write("\n")
print("--- Time spent writing output: %s seconds ---" % (time.time() - start_time))

print("Untouched: {}".format(len(nothing)))
print("Updated: {}".format(len(update)))
print("Replacee: {}".format(len(replace)))
print("Replacer: {}".format(len(replaced)))
