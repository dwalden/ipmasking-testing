# Copyright (C) 2023 Dominic Walden

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
# http://www.gnu.org/copyleft/gpl.html

class Spur:

    def __init__(self, data):
        self.data = data
        self.ip = ""
        self.aso = None
        self.asn = None
        self.org = None
        self.location_country = ""
        self.location_city = ""
        self.location_state = ""
        self.conc_country = ""
        self.conc_city = ""
        self.conc_state = ""
        self.countries = 0
        self.client_count = 0
        self.types = []
        self.risks = []
        self.behaviors = []
        self.proxies = []
        self.tunnels = []

        if 'ip' in data:
            self.ip = str(data['ip'])
        if 'organization' in data:
            self.org = data['organization']

        if 'as' in data:
            if 'number' in data['as']:
                self.asn = data['as']['number']
            if 'organization' in data['as']:
                self.aso = data['as']['organization']

        if 'location' in data:
            if 'country' in data['location']:
                self.location_country = data['location']['country']
            if 'city' in data['location']:
                self.location_city = data['location']['city']
            if 'state' in data['location']:
                self.location_state = data['location']['state']

        if 'client' in data and type(data['client']) is dict:
            if 'countries' in data['client']:
                self.countries = data['client']['countries']
            if 'concentration' in data['client']:
                if 'country' in data['client']['concentration']:
                    self.conc_country = data['client']['concentration']['country']
                if 'city' in data['client']['concentration']:
                    self.conc_city = data['client']['concentration']['city']
                if 'state' in data['client']['concentration']:
                    self.conc_state = data['client']['concentration']['state']

            if 'count' in data['client'] and type(data['client']['count']) is int:
                self.client_count = data['client']['count']

            if 'behaviors' in data['client'] and type(data['client']['behaviors']) is list:
                for behavior in data['client']['behaviors']:
                    if type(behavior) is str:
                        self.behaviors.append(behavior)
                self.behaviors = sorted(list(set(self.behaviors)))

            if 'proxies' in data['client'] and type(data['client']['proxies']) is list:
                for proxy in data['client']['proxies']:
                    if type(proxy) is str:
                        self.proxies.append(proxy)
                self.proxies = sorted(list(set(self.proxies)))

            if 'types' in data['client'] and type(data['client']['types']) is list:
                for typpe in data['client']['types']:
                    if type(typpe) is str:
                        self.types.append(typpe)
                self.types = sorted(list(set(self.types)))

        if 'risks' in data and type(data['risks']) is list:
            for risk in data['risks']:
                if type(risk) is str:
                    self.risks.append(risk)
            self.risks = sorted(list(set(self.risks)))

        if 'tunnels' in data and type(data['tunnels']) is list:
            for tunnel in data['tunnels']:
                if type(tunnel) is dict and 'operator' in tunnel and type(tunnel['operator']) is str:
                    self.tunnels.append({
                        "operator": tunnel['operator'],
                        "type": None if 'type' not in tunnel or type(tunnel['type']) is not str else tunnel['type'],
                        "anonymous": None if 'anonymous' not in tunnel or (type(tunnel['anonymous']) is not int and type(tunnel['anonymous']) is not bool) else int(tunnel['anonymous'])
                    })
            self.tunnels = sorted(self.tunnels, key=lambda d: d['operator'] or '')
