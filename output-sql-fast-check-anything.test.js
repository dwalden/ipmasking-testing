'use strict';

const fc = require('fast-check');

const sqlString = require( 'sqlstring' );
const { getActorTypes, getActorRisks, getTunnels, actorTypes, riskTypes, getActorProxies, getActorBehaviors } = require( '../../../import-data-utils' );

fc.configureGlobal({ numRuns: 100000 });

// behaviours, countries(!), proxies, types, risks
function arrayCompare( arr1, arr2 ) {
	if ( arr1 === arr2 ) {
		return true;
	}
	if ( !arr1 || !arr2 ) {
		return false;
	}
	if ( arr1.length !== arr2.length ) {
		return false;
	}
	for ( let i = 0; i < arr1.length; i++ ) {
		if ( arr1[ i ] !== arr2[ i ] ) {
			return false;
		}
	}
	return true;
}

function tunnelsCompare( tunnel1, tunnel2 ) {
	if ( tunnel1.operator !== tunnel2.operator ) {
		return false;
	}
	if ( tunnel1.type !== tunnel2.type ) {
		return false;
	}
	return true;
}

function tunnelsSort( tunnel1, tunnel2 ) {
	if ( tunnel1.operator < tunnel2.operator ) {
		return -1;
	}
	if ( tunnel1.operator > tunnel2.operator ) {
		return 1;
	}
	return 0;
}

function tunnelsArrayCompare( arr1, arr2 ) {
	if ( arr1 === arr2 ) {
		return true;
	}
	if ( !arr1 || !arr2 ) {
		return false;
	}
	if ( arr1.length !== arr2.length ) {
		return false;
	}
	arr1.sort( tunnelsSort );
	arr2.sort( tunnelsSort );
	for ( let i = 0; i < arr1.length; i++ ) {
		if ( !tunnelsCompare( arr1[ i ], arr2[ i ] ) ) {
			return false;
		}
	}
	return true;
}

function checkIfChanged( dataPrev, dataCurr ) {
	// IPs should be the same if we got here

	if ( dataPrev.organization !== dataCurr.organization ) {
		return true;
	}
	if ( !!dataPrev.client !== !!dataCurr.client ) {
		return true;
	}
	if ( dataPrev.client ) {
		// client exists on both
		if ( !arrayCompare( dataPrev.client.behaviors, dataCurr.client.behaviors ) ) {
			return true;
		}
		if ( !!dataPrev.client.concentration !== !!dataCurr.client.concentration ) {
			return true;
		}
		if ( dataPrev.client.concentration ) {
			// client.concentration exists on both
			if (
				dataPrev.client.concentration.city !==
					dataCurr.client.concentration.city
			) {
				return true;
			}
			if (
				dataPrev.client.concentration.country !==
					dataCurr.client.concentration.country
			) {
				return true;
			}
			if (
				dataPrev.client.concentration.state !==
					dataCurr.client.concentration.state
			) {
				return true;
			}
		}
		if ( dataPrev.client.count !== dataCurr.client.count ) {
			return true;
		}
		if ( !arrayCompare( dataPrev.client.countries, dataCurr.client.countries ) ) {
			return true;
		}
		if ( !arrayCompare( dataPrev.client.proxies, dataCurr.client.proxies ) ) {
			return true;
		}
		if ( !arrayCompare( dataPrev.client.types, dataCurr.client.types ) ) {
			return true;
		}
	}
	if ( !!dataPrev.location !== !!dataCurr.location ) {
		return true;
	}
	if ( dataPrev.location ) {
		// location exists on both
		if ( dataPrev.location.country !== dataCurr.location.country ) {
			return true;
		}
	}
	if ( !arrayCompare( dataPrev.risks, dataCurr.risks ) ) {
		return true;
	}
	if ( !!dataPrev.tunnels !== !!dataCurr.tunnels ) {
		return true;
	}
	if ( dataPrev.tunnels ) {
		// tunnels exists on both
		if ( !tunnelsArrayCompare( dataPrev.tunnels, dataCurr.tunnels ) ) {
			return true;
		}
	}
	return false;
}

function returnAsEncapsulatedString( array ) {
	return array.map( function ( item ) {
		return sqlString.escape( item );
	} ).join( ',' );
}

function generateInsertActorQueries( actor ) {
	const actorObj = {
		actor_data: {
			ip: actor.ip,
			org: actor.organization,
			client_count: actor.client.count || 0,
			types: actor.client.types ?
				getActorTypes( actor.client.types ) : actorTypes.UNKNOWN,
			conc_city:
			actor.client.concentration && actor.client.concentration.city ? actor.client.concentration.city : '',
			conc_state:
			actor.client.concentration && actor.client.concentration.state ? actor.client.concentration.state : '',
			conc_country:
			actor.client.concentration && actor.client.concentration.country ? actor.client.concentration.country : '',
			countries: actor.client.countries || 0,
			location_country: actor.location.country || '',
			risks: actor.risks ? getActorRisks( actor.risks ) : riskTypes.UNKNOWN
		},
		behaviors: actor.client.behaviors && Array.isArray( actor.client.behaviors ) ?
			getActorBehaviors( actor.client.behaviors ) : [],
		proxies: actor.client.proxies && Array.isArray( actor.client.proxies ) ?
			getActorProxies( actor.client.proxies ) : [],
		tunnels: actor.tunnels && Array.isArray( actor.tunnels ) ?
			getTunnels( actor.tunnels ) : false
	};

	let queries = '';
	queries += `INSERT INTO actor_data (${Object.keys( actorObj.actor_data ).toString()}) VALUES (${returnAsEncapsulatedString( Object.values( actorObj.actor_data ) )});@@@@@`;
	actorObj.behaviors.forEach( function ( behavior ) {
		queries += `INSERT INTO actor_data_behaviors (actor_data_id, behavior_id) VALUES( (SELECT pkid FROM actor_data WHERE ip = '${actorObj.actor_data.ip}'), (SELECT pkid FROM behaviors WHERE behavior = '${behavior}') );@@@@@`;
	} );
	actorObj.proxies.forEach( function ( proxy ) {
		queries += `INSERT INTO actor_data_proxies (actor_data_id, proxy_id) VALUES( (SELECT pkid FROM actor_data WHERE ip = '${actorObj.actor_data.ip}'), (SELECT pkid FROM proxies WHERE proxy = '${proxy}' ) );@@@@@`;
	} );
	if ( actorObj.tunnels ) {
		actorObj.tunnels.forEach( function ( tunnel ) {
			queries += `INSERT INTO actor_data_tunnels (actor_data_id, tunnel_id) VALUES( (SELECT pkid FROM actor_data WHERE ip = '${actorObj.actor_data.ip}'), (SELECT pkid FROM tunnels WHERE operator = '${tunnel.operator}' ) );@@@@@`;
		} );
	}
	return queries;
}

var spur = fc.record(
	{
		as: fc.anything(),
		client: fc.anything(),
		infrastructure: fc.anything(),
		location: fc.anything(),
		organization: fc.anything(),
		risks: fc.anything(),
		services: fc.anything(),
		tunnels: fc.anything()
	},
	{ requiredKeys: [] }
);

describe( 'output-sql.js', function () {
	it( 'arrayCompare', function () {
		fc.assert(
			fc.property( fc.anything(), fc.anything(), function ( array1, array2 ) {
				try {
					arrayCompare( array1, array2 );
					arrayCompare( array1, array1 );
					arrayCompare( array2, array2 );
					return true;
				} catch ( error ) {
					console.log( error );
					console.log( array1, array2 );
					return false;
				}
			})
		);
	});

	it( 'tunnelsCompare', function () {
		fc.assert(
			fc.property( fc.anything(), fc.anything(), function ( array1, array2 ) {
				try {
					tunnelsCompare( array1, array2 );
					tunnelsCompare( array1, array1 );
					tunnelsCompare( array2, array2 );
					return true;
				} catch ( error ) {
					console.log( error );
					console.log( array1, array2 );
					return false;
				}
			})
		);
	});

	it( 'tunnelsSort', function () {
		fc.assert(
			fc.property( fc.anything(), fc.anything(), function ( array1, array2 ) {
				try {
					tunnelsSort( array1, array2 );
					tunnelsSort( array1, array1 );
					tunnelsSort( array2, array2 );
					return true;
				} catch ( error ) {
					console.log( error );
					console.log( array1, array2 );
					return false;
				}
			})
		);
	});

	it( 'tunnelsArrayCompare', function () {
		fc.assert(
			fc.property( fc.anything(), fc.anything(), function ( array1, array2 ) {
				try {
					tunnelsArrayCompare( array1, array2 );
					tunnelsArrayCompare( array1, array1 );
					tunnelsArrayCompare( array2, array2 );
					return true;
				} catch ( error ) {
					console.log( error );
					console.log( array1, array2 );
					return false;
				}
			})
		);
	});

	it( 'checkIfChanged', function () {
		fc.assert(
			fc.property( spur, spur, function ( array1, array2 ) {
				try {
					checkIfChanged( array1, array2 );
					checkIfChanged( array1, array1 );
					checkIfChanged( array2, array2 );
					return true;
				} catch ( error ) {
					console.log( error );
					console.log( array1, array2 );
					return false;
				}
			})
		);
	});

	it( 'generateInsertActorQueries', function () {
		fc.assert(
			fc.property( spur, function ( array ) {
				try {
					generateInsertActorQueries( array );
					return true;
				} catch ( error ) {
					console.log( error );
					console.log( array );
					return false;
				}
			})
		);
	});
});
