# Copyright (C) 2023 Dominic Walden

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
# http://www.gnu.org/copyleft/gpl.html

# Usage:
# python3 spur_random_data_valid.py -n 1000

import json
# import ipaddress
import uuid
import string
import argparse

from hypothesis import given, settings
import hypothesis.strategies as st


args = argparse.ArgumentParser()
args.add_argument('-n', '--num', type=int, default=1000,
                  help="Number of test entries to generate.")
options = args.parse_args()


def text_list(max_size):
    return st.one_of(st.none(), st.just(False), st.lists(st.text(alphabet=string.ascii_letters, min_size=1, max_size=max_size)))


text_atom = st.one_of(st.none(), st.just(False), st.text(alphabet=string.ascii_letters, min_size=1))
integers_atom = st.one_of(st.none(), st.just(False), st.integers(min_value=0))
booleans_atom = st.one_of(st.none(), st.booleans())

as_strat = st.fixed_dictionaries({},
                                 optional={'organization': text_atom,
                                           'number': integers_atom})

concentration_strat = st.fixed_dictionaries({},
                                            optional={'city': text_atom,
                                                      'country': text_atom,
                                                      'density': integers_atom,
                                                      'geohash': text_atom,
                                                      'skew': st.floats(0, 1),
                                                      'state': text_atom})

client_strat = st.fixed_dictionaries({},
                                     optional={'behaviors': text_list(64),
                                               'concentration': concentration_strat,
                                               'count': integers_atom,
                                               'countries': integers_atom,
                                               'proxies': text_list(128),
                                               'spread': integers_atom,
                                               'types': st.one_of(st.none(), st.booleans(), st.lists(st.sampled_from(['DESKTOP', 'HEADLESS', 'IOT', 'MOBILE']), min_size=0))})

location_strat = st.dictionaries(st.sampled_from(['city', 'country', 'state']), text_atom)

tunnels_strat = st.lists(st.fixed_dictionaries({'type': st.one_of(st.none(), st.just(False), st.text(alphabet=string.ascii_letters, min_size=1, max_size=5))},
                                               optional={'operator': text_atom,
                                                         'anonymous': booleans_atom,
                                                         'entries': text_list(10),
                                                         'exits': text_list(10)}))

strat = st.fixed_dictionaries({},
                              optional={'as': as_strat,
                                        'client': client_strat,
                                        'infrastructure': st.one_of(st.none(), st.booleans(), st.lists(st.sampled_from(['DATACENTER', 'MOBILE', 'SATELLITE', 'IN_FLIGHT_WIFI', 'GOOGLE']), min_size=0)),
                                        # 'ip': st.uuids(), # st.one_of(st.booleans(), text_atom, st.characters(), integers_atom, st.none(), st.ip_addresses()),
                                        'location': location_strat,
                                        'organization': text_atom,
                                        'risks': st.one_of(st.none(), st.booleans(), st.lists(st.sampled_from(['CALLBACK_PROXY', 'GEO_MISMATCH', 'LOGIN_BRUTEFORCE', 'TUNNEL', 'WEB_SCRAPING']), min_size=0)),
                                        'services': st.one_of(st.none(), st.booleans(), st.lists(st.text())),
                                        'tunnels': tunnels_strat})


@given(strat)
@settings(max_examples=options.num)
def generate_test_data(d):
    # if 'ip' in d and type(d['ip']) is uuid.UUID: # (type(d['ip']) is ipaddress.IPv4Address or type(d['ip']) is ipaddress.IPv6Address):
    #     d['ip'] = str(d['ip'])
    d['ip'] = str(uuid.uuid4())
    print(json.dumps(d))


if __name__ == "__main__":
    generate_test_data()
