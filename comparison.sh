#!/bin/bash

# Copyright (C) 2023 Dominic Walden

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
# http://www.gnu.org/copyleft/gpl.html

# This script randomly shuffles a JSON input file, imports it into ipoid, then
# compares the data in the database and the number of lines in the error between
# successive runs. Because we are only changing the order of the data, the
# errors and the database should not be different between runs.

# Example usage:
# 1. Create a JSON file
# 2. Download https://gitlab.wikimedia.org/dwalden/ipmasking-testing/-/blob/main/query.sql
# 3. The script should be run from the ipoid directory:
#    bash comparison.sh <json file> <num runs> /path/to/query.sql

infile=$1
num=$2
query=$3
bsn=$(basename $infile)

for i in $(seq 1 $num)
do
    rndname=$bsn"_shuf"$i".json"
    echo $rndname
    shuf $infile > $rndname
    gzip -k $rndname
    mv $rndname".gz" tmp/
    docker-compose exec web node init-db.js
    docker-compose exec web node import-data.js ./tmp/$rndname".gz" > $bsn"_shuf"$i"_error.txt"
    mysql test -h 172.17.0.1 -P 3308 -u root -pexample < $query > $bsn"_shuf"$i"_db.txt"
done

for i in $(seq 2 $num)
do
    echo "Comparing "$bsn"_shuf1 with "$bsn"_shuf"$i
    diff -q $bsn"_shuf1_db.txt" $bsn"_shuf"$i"_db.txt"
    one=$(wc -l < $bsn"_shuf1_error.txt")
    two=$(wc -l < $bsn"_shuf"$i"_error.txt")
    if [ ! $one -eq $two ]
    then
	echo "Error files are not same length"
    fi
done
