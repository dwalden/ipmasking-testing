# Copyright (C) 2023 Dominic Walden

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
# http://www.gnu.org/copyleft/gpl.html

# Example usage:
# python3 ipoid_data_validation_1.py -f feed.v2.test.json

import argparse
import json
import pymysql


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def standard_checks(dictionary, parent_key, child_key):
    if parent_key not in dictionary or dictionary[parent_key] is None or type(dictionary[parent_key]) is not dict or child_key not in dictionary[parent_key]:
        return True
    else:
        return False


def standard_checks_conc(dictionary, key):
    if standard_checks(dictionary, 'client', 'concentration') or type(dictionary['client']['concentration']) is not dict or key not in dictionary['client']['concentration']:
        return True
    else:
        return False


def match(left, right, label):
    if left != right:
        print("{} don't match: {} {}".format(label, left, right))


def extract_list(lst):
    foo = str.encode(",".join(sorted(list(set(["" if type(item) is not str else item for item in lst])))))
    if foo == b"":
        return None
    else:
        return foo


def extract_tunnels(foo):
    if 'tunnels' not in foo or type(foo['tunnels']) is not list or len(foo['tunnels']) == 0:
        return None
    else:
        tunnels = b""
        for item in foo['tunnels']:
            if 'operator' in item and type(item) is dict:
                if tunnels != b"":
                    tunnels += ","
                tunnels += str.encode("{} {} {}".format(item['operator'], "" if 'type' not in item else item['type'], 0 if 'anonymous' not in item or (type(item['anonymous']) is not int and type(item['anonymous']) is not bool) else int(item['anonymous'])))
    if tunnels == b"":
        return None
    else:
        return tunnels


args = argparse.ArgumentParser()
args.add_argument('-f', '--file', required=True)
args.add_argument('-s', '--server', default="172.17.0.1")
args.add_argument('-P', '--port', type=int, default=3306)
args.add_argument('-u', '--user', default="root")
args.add_argument('-p', '--password', default="example")
args.add_argument('-d', '--database', default="test")
options = args.parse_args()

# TODO: Change types, risks and tunnels when we support having multiple values
# for these (T339204)
query = """
SELECT ad.ip, ad.org, ad.client_count, ad.types, ad.conc_geohash, ad.conc_city, ad.conc_state,
       ad.conc_country, ad.conc_skew, ad.conc_density, ad.countries, ad.location_country, ad.risks,
       GROUP_CONCAT(DISTINCT b.behavior) AS behaviors, GROUP_CONCAT(DISTINCT p.proxy) AS proxies,
       GROUP_CONCAT(DISTINCT CONCAT(t.operator, " ", t.type, " ", t.anonymous)) AS tunnels
FROM actor_data AS ad
# Behaviours
LEFT JOIN actor_data_behaviors AS adb ON ad.pkid=adb.actor_data_id
LEFT JOIN behaviors AS b ON adb.behavior_id=b.pkid
# Proxies
LEFT JOIN actor_data_proxies AS adp ON ad.pkid=adp.actor_data_id
LEFT JOIN proxies AS p ON adp.proxy_id=p.pkid
# Tunnels
LEFT JOIN actor_data_tunnels AS adt ON ad.pkid=adt.actor_data_id
LEFT JOIN tunnels AS t ON adt.tunnel_id=t.pkid
GROUP BY ad.pkid;
"""

connection = pymysql.connect(host=options.server,
                             port=options.port,
                             user=options.user,
                             password=options.password,
                             database=options.database,
                             cursorclass=pymysql.cursors.DictCursor)
cursor = connection.cursor()
cursor.execute(query)
result = cursor.fetchall()

# Initialise the "expected" values. Use defaults if, for example, values don't
# exist. Also store the "raw" JSON.
json_comp = {}
raw_json = {}
with open(options.file, "r") as fp:
    for line in fp:
        try:
            foo = json.loads(line)
            json_comp["" if 'ip' not in foo else str(foo['ip'])] = {
                "ip": "" if 'ip' not in foo else foo['ip'] if type(foo['ip']) is not str else str.encode(foo['ip']),
                "org": b"" if 'organization' not in foo or type(foo['organization']) is not str else str.encode(foo['organization']),
                "client_count": 0 if standard_checks(foo, 'client', 'count') or type(foo['client']['count']) is not int else foo['client']['count'],
                "types": "UNKNOWN" if standard_checks(foo, 'client', 'types') or type(foo['client']['types']) is not list else extract_list(foo['client']['types']),
                "conc_geohash": b"" if standard_checks_conc(foo, 'geohash') or type(foo['client']['concentration']['geohash']) is not str else str.encode(foo['client']['concentration']['geohash']),
                "conc_city": b"" if standard_checks_conc(foo, 'city') or type(foo['client']['concentration']['city']) is not str else str.encode(foo['client']['concentration']['city']),
                "conc_state": b"" if standard_checks_conc(foo, 'state') or type(foo['client']['concentration']['state']) is not str else str.encode(foo['client']['concentration']['state']),
                "conc_country": b"" if standard_checks_conc(foo, 'country') or type(foo['client']['concentration']['country']) is not str else str.encode(foo['client']['concentration']['country']),
                "conc_skew": 0 if standard_checks_conc(foo, 'skew') or (type(foo['client']['concentration']['skew']) is not int and type(foo['client']['concentration']['skew']) is not float) else foo['client']['concentration']['skew'],
                "conc_density": 0 if standard_checks_conc(foo, 'density') or (type(foo['client']['concentration']['density']) is not int and type(foo['client']['concentration']['density']) is not float) else foo['client']['concentration']['density'],
                "countries": 0 if standard_checks(foo, 'client', 'countries') or type(foo['client']['countries']) is not int else foo['client']['countries'],
                "location_country": b"" if standard_checks(foo, 'location', 'country') or type(foo['location']['country']) is not str else str.encode(foo['location']['country']),
                "risks": "UNKNOWN" if 'risks' not in foo or type(foo['risks']) is not list else ",".join(sorted(list(set(["" if type(item) is not str else item for item in foo['risks']])))),
                "behaviors": None if standard_checks(foo, 'client', 'behaviors') or type(foo['client']['behaviors']) is not list else extract_list(foo['client']['behaviors']),
                "proxies": None if standard_checks(foo, 'client', 'proxies') or type(foo['client']['proxies']) is not list else extract_list(foo['client']['proxies']),
                "tunnels": extract_tunnels(foo)
            }
            raw_json["" if 'ip' not in foo else str(foo['ip'])] = line
        except:
            print("{}Exception parsing {}{}".format(bcolors.HEADER, line, bcolors.ENDC))

# Compare results from SQL with JSON
end_results = {}
for ip in result:
    if ip['ip'].decode() in json_comp:
        if ip != json_comp[ip['ip'].decode()]:
            print("{}=== {} does not match ==={}".format(bcolors.WARNING, ip['ip'].decode(), bcolors.ENDC))
            print("       Raw JSON: {}".format(raw_json[ip['ip'].decode()]))
            print("Normalised JSON: {}".format(json_comp[ip['ip'].decode()]))
            print("            SQL: {}".format(ip))
        for var, value in ip.items():
            if value != json_comp[ip['ip'].decode()][var]:
                print("{}{} {} don't match: {} {}{}".format(bcolors.FAIL, ip['ip'].decode(), var, value, json_comp[ip['ip'].decode()][var], bcolors.ENDC))
                if var not in end_results:
                    end_results[var] = [ip['ip'].decode()]
                else:
                    end_results[var].append(ip['ip'].decode())
        del json_comp[ip['ip'].decode()]
    else:
        print("{} not found".format(ip['ip'].decode()))

if len(json_comp) > 0:
    print("IPs in JSON that were not saved to the database:")
    print(json_comp.keys())

print("==== Variables not matching ====")
for var, ips in end_results.items():
    print("{}: {}".format(var, len(ips)))
