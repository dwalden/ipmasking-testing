# Copyright (C) 2023 Dominic Walden

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
# http://www.gnu.org/copyleft/gpl.html

import argparse
import json
from tabulate import tabulate
from spur import Spur
import geoip2.database

args = argparse.ArgumentParser()
args.add_argument('-a', '--anon', required=True)
args.add_argument('-e', '--enterprise', required=True)
args.add_argument('-s', '--spur', required=True)
options = args.parse_args()

with geoip2.database.Reader(options.enterprise) as enterprise, geoip2.database.Reader(options.anon) as anon, open(options.spur, "r") as spur_json:
    for line in spur_json:
        spur = Spur(json.loads(line))
        try:
            ip_enterprise = enterprise.enterprise(spur.ip)
            ip_anon = anon.anonymous_ip(spur.ip)
        except:
            print("IP not found in MaxMind: {}".format(spur.ip))
            continue
        if ip_enterprise.traits.autonomous_system_number != spur.asn and spur.location_country != ip_enterprise.country.iso_code:
            print("== {} ASN does not match ==".format(spur.ip))
            table = [
                ["ASN", spur.asn, ip_enterprise.traits.autonomous_system_number],
                ["Organisation", spur.org, ip_enterprise.traits.organization],
                ["ASO", spur.aso, ip_enterprise.traits.autonomous_system_organization],
                ["Location Country", spur.location_country, ip_enterprise.country.iso_code],
                ["Location City", spur.location_city, ip_enterprise.city.name],
                ["Behaviors", str(spur.behaviors), ""],
                ["Proxies", str(spur.proxies), ""],
                ["Tunnels", str(spur.tunnels), ""],
                ["Anon", "", ip_anon.is_anonymous],
                ["VPN", "", ip_anon.is_anonymous_vpn],
                ["Hosting", "", ip_anon.is_hosting_provider],
                ["Public Proxy", "", ip_anon.is_public_proxy],
                ["Residential Proxy", "", ip_anon.is_residential_proxy],
                ["Tor Exit Node", "", ip_anon.is_tor_exit_node]
            ]
            print(tabulate(table, header=["Data", "Spur", "Maxmind"]))
