# Copyright (C) 2023 Dominic Walden

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
# http://www.gnu.org/copyleft/gpl.html


import json
import uuid


def extract(dictionary, prefix = ""):
    keys = []
    for key, value in dictionary.items():
        keys.append("{}{}".format(prefix, key))
        if type(value) is dict:
            keys.extend(extract(value, "{}{},".format(prefix, key)))
        elif type(value) is list:
            for i in value:
                if type(i) is dict:
                    keys.extend(extract(i, "{}{},".format(prefix, key)))
    return keys


for t in [None, False, True, 0, 1, ""]:
    print(json.dumps({"as": {"number": t, "Organization": t}, "client": {"behaviors": [t], "concentration": {"geohash": t, "city": t, "state": t, "country": t, "skew": t if type(t) is not int or int(t) < 2**32 else 0, "density": t if type(t) is not int or int(t) < 2**32 else 0, "countries": t}, "count": t if type(t) is not int or int(t) < 2**32 else 0, "countries": t if type(t) is not int or int(t) < 2**32 else 0, "proxies": [t], "spread": t, "types": ["MOBILE"]}, "infrastructure": t, "location": {"city": t, "state": t, "country": t}, "organization": t, "risks": ["CALLBACK_PROXY"], "services": [t], "tunnels": [{"operator": t, "type": t, "anonymous": t}], "ip": str(uuid.uuid4())}))


valid = {"as": {"number": 12345, "Organization": "Foobar"}, "client": {"behaviors": ["FILE_SHARING"], "concentration": {"geohash": "srwcr5ugt", "city": "Foobar", "state": "Foobar", "country": "NL", "skew": 0.5, "density": 0.5, "countries": 10}, "count": 10, "countries": 10, "proxies": ["OXYLABS_PROXY"], "spread": 12345, "types": ["MOBILE"]}, "infrastructure": "MOBILE", "location": {"city":"Baku","state":"Baku City","country":"AZ"}, "organization": "Foobar", "risks": ["CALLBACK_PROXY"], "services": ["IPSEC"], "tunnels": [{"operator":"TUNNELBEAR_VPN","type":"VPN","anonymous":True}], "ip": str(uuid.uuid4())}

for var in extract(valid):
    for t in [None, False, True, 0, 1, "", [None], [False], [True], [0], [1], [""], "del"]:
        foo = var.split(",")
        if len(foo) == 1:
            if t == "del":
                del valid[foo[0]]
            else:
                valid[foo[0]] = t
        elif len(foo) == 2:
            if foo[0] == "tunnels":
                if t == "del":
                    del valid[foo[0]][0][foo[1]]
                else:
                    valid[foo[0]][0][foo[1]] = t
            else:
                if t == "del":
                    del valid[foo[0]][foo[1]]
                else:
                    valid[foo[0]][foo[1]] = t
        elif len(foo) == 3:
            if t == "del":
                del valid[foo[0]][foo[1]][foo[2]]
            else:
                valid[foo[0]][foo[1]][foo[2]] = t
        print(json.dumps(valid))
        valid = {"as": {"number": 12345, "Organization": "Foobar"}, "client": {"behaviors": ["FILE_SHARING"], "concentration": {"geohash": "srwcr5ugt", "city": "Foobar", "state": "Foobar", "country": "NL", "skew": 0.5, "density": 0.5, "countries": 10}, "count": 10, "countries": 10, "proxies": ["OXYLABS_PROXY"], "spread": 12345, "types": ["MOBILE"]}, "infrastructure": "MOBILE", "location": {"city":"Baku","state":"Baku City","country":"AZ"}, "organization": "Foobar", "risks": ["CALLBACK_PROXY"], "services": ["IPSEC"], "tunnels": [{"operator":"TUNNELBEAR_VPN","type":"VPN","anonymous":True}], "ip": str(uuid.uuid4())}
