# Copyright (C) 2023 Dominic Walden

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
# http://www.gnu.org/copyleft/gpl.html

# Example usage:
# python3 ipoid_data_validation.py -f feed.v2.test.json

import argparse
import json
import pymysql

entity_types = [
    "UNKNOWN",
    "DESKTOP",
    "HEADLESS",
    "IOT",
    "MOBILE"
]

risk_types = [
    "UNKNOWN",
    "CALLBACK_PROXY",
    "GEO_MISMATCH",
    "LOGIN_BRUTEFORCE",
    "TUNNEL",
    "WEB_SCRAPING"
]

default = {
    "org": "",
    "client_count": 0,
    "types": "",
    "conc_geohash": "",
    "conc_city": "",
    "conc_country": "",
    "conc_skew": 0,
    "conc_density": 0,
    "countries": 0,
    "location_country": "",
    "risks": "",
    "behaviors": None,
    "proxies": None,
    "tunnels": None
}


def match(left, right, label):
    if left != right:
        print("{} don't match: {} {}".format(label, left, right))


def compare(left, right):
    match(left['org'] if left['org'] is None else left['org'].decode(), "" if 'organization' not in right else str(right['organization']), "{} organisation".format(left['ip'].decode()))

    match(left['client_count'], 0 if type(right['client']) is not dict or 'count' not in right['client'] else right['client']['count'], "{} client count".format(left['ip'].decode()))

    match("" if left['types'] == "" else [entity_types[int(i)] for i in str(left['types']).split(",")], right['client'] if type(right['client']) is not dict or 'types' not in right['client'] else sorted(list(set(right['client']['types']))), "{} types".format(left['ip'].decode()))

    if type(right['client']) is dict and 'concentration' in right['client']:
        match(left['conc_geohash'].decode(), "" if 'geohash' not in right['client']['concentration'] or type(right['client']['concentration']['geohash']) is not str else right['client']['concentration']['geohash'], "{} geohash".format(left['ip'].decode()))
        match(left['conc_city'].decode(), "" if 'city' not in right['client']['concentration'] or type(right['client']['concentration']['city']) is not str else right['client']['concentration']['city'], "{} city".format(left['ip'].decode()))
        match(left['conc_state'].decode(), "" if 'state' not in right['client']['concentration'] or type(right['client']['concentration']['state']) is not str else right['client']['concentration']['state'], "{} state".format(left['ip'].decode()))
        match(left['conc_country'].decode(), "" if 'country' not in right['client']['concentration'] or type(right['client']['concentration']['country']) is not str else right['client']['concentration']['country'], "{} country".format(left['ip'].decode()))
        match(left['conc_skew'], 0 if 'skew' not in right['client']['concentration'] else right['client']['concentration']['skew'], "{} skew".format(left['ip'].decode()))
        match(left['conc_density'], 0 if 'density' not in right['client']['concentration'] else right['client']['concentration']['density'], "{} density".format(left['ip'].decode()))

    match(left['countries'], 0 if 'countries' not in right['client'] or type(right['client']['countries']) is not int else right['client']['countries'], "{} countries".format(left['ip'].decode()))

    match(left['location_country'].decode(), "" if 'country' not in right['location'] else right['location']['country'], "{} location_country".format(left['ip'].decode()))

    match([] if left['risks'].split(",") == [''] else [risk_types[int(i)] for i in left['risks'].split(",")], [] if 'risks' not in right else sorted(list(set(right['risks']))), "{} risks".format(left['ip'].decode()))

    match("" if left['behaviours'] is None else left['behaviours'].decode(), ",".join("" if 'behaviors' not in right['client'] else sorted([str(i) for i in right['client']['behaviors']])), "{} behaviours".format(left['ip'].decode()))

    match("" if left['proxies'] is None else left['proxies'].decode(), "" if 'proxies' not in right['client'] else ",".join(sorted([str(i) for i in right['client']['proxies']])), "{} proxies".format(left['ip'].decode()))

    match("" if left['tunnels'] is None else left['tunnels'].decode(), "" if 'tunnels' not in right or type(right['tunnels']) is not list else ",".join(["{} {} {}".format("" if 'operator' not in tunnel else str(tunnel['operator']), "" if 'type' not in tunnel else str(tunnel['type']), "" if 'anonymous' not in tunnel or tunnel['anonymous'] is None or tunnel['anonymous'] == "" else int(tunnel['anonymous'])) for tunnel in right['tunnels']]), "{} tunnels".format(left['ip'].decode()))


args = argparse.ArgumentParser()
args.add_argument('-f', '--file', required=True)
args.add_argument('-s', '--server', default="172.17.0.1")
args.add_argument('-P', '--port', type=int, default=3306)
args.add_argument('-u', '--user', default="root")
args.add_argument('-p', '--password', default="example")
args.add_argument('-d', '--database', default="test")
options = args.parse_args()

query = """
SELECT ad.*, GROUP_CONCAT(DISTINCT b.behavior) AS behaviours, GROUP_CONCAT(DISTINCT p.proxy) AS proxies,
       GROUP_CONCAT(DISTINCT CONCAT(t.operator, " ", t.type, " ", t.anonymous)) AS tunnels
FROM actor_data AS ad
# Behaviours
LEFT JOIN actor_data_behaviors AS adb ON ad.pkid=adb.actor_data_id
LEFT JOIN behaviors AS b ON adb.behavior_id=b.pkid
# Proxies
LEFT JOIN actor_data_proxies AS adp ON ad.pkid=adp.actor_data_id
LEFT JOIN proxies AS p ON adp.proxy_id=p.pkid
# Tunnels
LEFT JOIN actor_data_tunnels AS adt ON ad.pkid=adt.actor_data_id
LEFT JOIN tunnels AS t ON adt.tunnel_id=t.pkid
GROUP BY ad.pkid;
"""

connection = pymysql.connect(host=options.server,
                             port=options.port,
                             user=options.user,
                             password=options.password,
                             database=options.database,
                             cursorclass=pymysql.cursors.DictCursor)
cursor = connection.cursor()
cursor.execute(query)
result = cursor.fetchall()

json_comp = {}
with open(options.file, "r") as fp:
    for line in fp:
        foo = json.loads(line)
        json_comp[foo['ip']] = foo

for ip in result:
    if ip['ip'].decode() in json_comp:
        compare(ip, json_comp[ip['ip'].decode()])
        del json_comp[ip['ip'].decode()]
    else:
        print("{} not found".format(ip['ip'].decode()))

if len(json_comp) > 0:
    print("IPs in JSON that were not saved to the database:")
    print(json_comp.keys())
