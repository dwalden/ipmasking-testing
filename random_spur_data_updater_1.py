# Copyright (C) 2023 Dominic Walden

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
# http://www.gnu.org/copyleft/gpl.html

# Example usage:
# python3 random_spur_data_updater.py -f input.json -s source.json -o output.json

import argparse
import json
import random
import time

args = argparse.ArgumentParser()
args.add_argument('-f', '--file', required=True, help="JSON feed you want updating.")
args.add_argument('-s', '--source', required=True, help="JSON feed you want to pull extra data from.")
args.add_argument('-o', '--output', required=True, help="Where you want to write the updated JSON feed.")
options = args.parse_args()


def get(spur, key):
    if key == 'ip' or key == 'risks' or key == 'tunnels':
        if key not in spur:
            return None
        else:
            return spur[key]
    elif key == 'org':
        if 'organization' not in spur:
            return None
        else: 
            return spur['organization']
    elif key == 'conc_country' or key == 'conc_city' or key == 'conc_state':
        if 'client' not in spur or 'concentration' not in spur['client'] or key[5:] not in spur['client']['concentration']:
            return None
        else:
            return spur['client']['concentration'][key[5:]]
    elif key == 'client_count':
        if 'client' not in spur or 'count' not in spur['client']:
            return None
        else:
            return spur['client']['count']
    elif key == 'countries':
        if 'client' not in spur or 'countries' not in spur['client']:
            return None
        else:
            return spur['client']['countries']
    elif key == 'proxies' or key == 'behaviors' or key == 'types':
        if 'client' not in spur or key not in spur['client']:
            return None
        else:
            return spur['client'][key]
    else:
        return None


def remove(spur, key):
    if key == 'ip' or key == 'risks' or key == 'tunnels':
        if key in spur:
            del spur[key]
    elif key == 'org':
        if 'organization' in spur:
            del spur['organization']
    elif key == 'conc_country' or key == 'conc_city' or key == 'conc_state':
        if 'client' in spur and 'concentration' in spur['client'] and key[5:] in spur['client']['concentration']:
            del spur['client']['concentration'][key[5:]]
    elif key == 'client_count':
        if 'client' in spur and 'count' in spur['client']:
            del spur['client']['count']
    elif key == 'countries' or key == 'proxies' or key == 'behaviors' or key == 'types':
        if 'client' in spur and key in spur['client']:
            del spur['client'][key]
    return spur


def update_spur(spur, key, value):
    if key == 'ip' or key == 'risks' or key == 'tunnels':
        spur[key] = value
    elif key == 'org':
        spur['organization'] = value
    elif key == 'conc_country' or key == 'conc_city' or key == 'conc_state':
        if 'client' not in spur:
            spur['client'] = {}
        if 'concentration' not in spur['client']:
            spur['client']['concentration'] = {}
        spur['client']['concentration'][key[5:]] = value
    elif key == 'client_count':
        if 'client' not in spur:
            spur['client'] = {}
        spur['client']['count'] = value
    elif key == "countries" or key == "proxies" or key == 'behaviors' or key == 'types':
        if 'client' not in spur:
            spur['client'] = {}
        spur['client'][key] = value
    return spur


final_json = []
update = []
nothing = []
replace = []
ips = set()
start_time = time.time()
with open(options.file, "r") as fp:
    for line in fp:
        line_json = json.loads(line)
        ips.add(line_json['ip'])
        choice = random.choice(["update"])
        if choice == "nothing":
            nothing.append(line_json['ip'])
            final_json.append(line_json)
        elif choice == "replace":
            replace.append(line_json['ip'])
        elif choice == "update":
            update.append(line_json)
print("--- Time spent processing input file: %s seconds ---" % (time.time() - start_time))

with open(options.source, "r") as source:
    start_time = time.time()
    replaced = []
    for i in range(0, len(replace)):
        rnd = random.randint(1, 100)
        j = 0
        for line in source:
            j = j + 1
            if j > rnd:
                line_json = json.loads(line)
                if line_json['ip'] not in ips:
                    replaced.append(line_json['ip'])
                    final_json.append(line_json)
                    break
    print("--- Time spent replacing entries: %s seconds ---" % (time.time() - start_time))

    start_time = time.time()
    for old in update:
        rnd = random.randint(1, 100)
        j = 0
        for line in source:
            j = j + 1
            if j > rnd:
                new = json.loads(line)
                keys = ["org", "conc_country", "conc_city", "conc_state", "countries", "client_count", "types", "risks", "behaviors", "proxies", "tunnels"]
                key = random.choice(keys)
                change = get(new, key)
                if change is not None:
                    old = update_spur(old, key, change)
                else:
                    old = remove(old, key)
                final_json.append(old)
                break
    print("--- Time spent updating entries: %s seconds ---" % (time.time() - start_time))

start_time = time.time()
with open(options.output, "a") as output: #, open("statistics_{}".format(options.output), "w") as stats:
    # json.dump({"unchanged": nothing, "updated": update, "replacee": replace, "replacer": replaced}, stats)
    for json_line in final_json:
        json.dump(json_line, output, separators=(',', ':'), ensure_ascii=False)
        output.write("\n")
print("--- Time spent writing output: %s seconds ---" % (time.time() - start_time))

print("Untouched: {}".format(len(nothing)))
print("Updated: {}".format(len(update)))
print("Replacee: {}".format(len(replace)))
print("Replacer: {}".format(len(replaced)))
