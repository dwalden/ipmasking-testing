# Copyright (C) 2023 Dominic Walden

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
# http://www.gnu.org/copyleft/gpl.html

# Usage:
# python3 spur_random_data.py

import json
# import ipaddress
import uuid
import argparse

from hypothesis import given, settings
import hypothesis.strategies as st

args = argparse.ArgumentParser()
args.add_argument('-n', '--num', type=int, default=1000,
                  help="Number of test entries to generate.")
options = args.parse_args()

test_atom = st.one_of(st.booleans(), st.text(), st.characters(), st.integers(), st.none())

as_strat = st.dictionaries(st.sampled_from(['Organization', 'number']), test_atom)

client_strat = st.fixed_dictionaries({'behaviors': st.lists(test_atom)},
                                     optional={'concentration': st.dictionaries(st.sampled_from(['city', 'country', 'density', 'geohash', 'skew', 'state']), test_atom),
                                               'count': test_atom,
                                               'countries': test_atom,
                                               'proxies': st.lists(test_atom),
                                               'spread': test_atom,
                                               'types': st.lists(st.sampled_from(['DESKTOP', 'HEADLESS', 'IOT', 'MOBILE']), min_size=1)})

location_strat = st.dictionaries(st.sampled_from(['city', 'country', 'state']), test_atom)

tunnels_strat = st.lists(st.fixed_dictionaries({'operator': test_atom},
                                               optional={'type': test_atom,
                                                         'anonymous': test_atom,
                                                         'entries': st.lists(test_atom)}))

strat = st.fixed_dictionaries({'as': as_strat},
                              optional={'client': client_strat,
                                        'infrastructure': test_atom,
                                        # 'ip': st.uuids(), # st.one_of(st.booleans(), st.text(), st.characters(), st.integers(), st.none(), st.ip_addresses()),
                                        'location': location_strat,
                                        'organization': test_atom,
                                        'risks': st.lists(st.sampled_from(['CALLBACK_PROXY', 'GEO_MISMATCH', 'LOGIN_BRUTEFORCE', 'TUNNEL', 'WEB_SCRAPING']), min_size=1),
                                        'services': st.lists(test_atom),
                                        'tunnels': tunnels_strat})


@given(strat)
@settings(max_examples=options.num)
def generate_test_data(d):
    # if 'ip' in d and type(d['ip']) is uuid.UUID: # (type(d['ip']) is ipaddress.IPv4Address or type(d['ip']) is ipaddress.IPv6Address):
    #     d['ip'] = str(d['ip'])
    d['ip'] = str(uuid.uuid4())
    print(json.dumps(d))


if __name__ == "__main__":
    generate_test_data()
