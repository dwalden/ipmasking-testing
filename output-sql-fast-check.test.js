'use strict';

const fc = require('fast-check');

const sqlString = require( 'sqlstring' );
const { getActorTypes, getActorRisks, getTunnels, actorTypes, riskTypes, getActorProxies, getActorBehaviors } = require( '../../../import-data-utils' );

fc.configureGlobal({ numRuns: 100000 });

// behaviours, countries(!), proxies, types, risks
function arrayCompare( arr1, arr2 ) {
	if ( arr1 === arr2 ) {
		return true;
	}
	if ( !arr1 || !arr2 ) {
		return false;
	}
	if ( arr1.length !== arr2.length ) {
		return false;
	}
	for ( let i = 0; i < arr1.length; i++ ) {
		if ( arr1[ i ] !== arr2[ i ] ) {
			return false;
		}
	}
	return true;
}

function tunnelsCompare( tunnel1, tunnel2 ) {
	if ( tunnel1.operator !== tunnel2.operator ) {
		return false;
	}
	if ( tunnel1.type !== tunnel2.type ) {
		return false;
	}
	return true;
}

function tunnelsSort( tunnel1, tunnel2 ) {
	if ( tunnel1.operator < tunnel2.operator ) {
		return -1;
	}
	if ( tunnel1.operator > tunnel2.operator ) {
		return 1;
	}
	return 0;
}

function tunnelsArrayCompare( arr1, arr2 ) {
	if ( arr1 === arr2 ) {
		return true;
	}
	if ( !arr1 || !arr2 ) {
		return false;
	}
	if ( arr1.length !== arr2.length ) {
		return false;
	}
	arr1.sort( tunnelsSort );
	arr2.sort( tunnelsSort );
	for ( let i = 0; i < arr1.length; i++ ) {
		if ( !tunnelsCompare( arr1[ i ], arr2[ i ] ) ) {
			return false;
		}
	}
	return true;
}

function checkIfChanged( dataPrev, dataCurr ) {
	// IPs should be the same if we got here

	if ( dataPrev.organization !== dataCurr.organization ) {
		return true;
	}
	if ( !!dataPrev.client !== !!dataCurr.client ) {
		return true;
	}
	if ( dataPrev.client ) {
		// client exists on both
		if ( !arrayCompare( dataPrev.client.behaviors, dataCurr.client.behaviors ) ) {
			return true;
		}
		if ( !!dataPrev.client.concentration !== !!dataCurr.client.concentration ) {
			return true;
		}
		if ( dataPrev.client.concentration ) {
			// client.concentration exists on both
			if (
				dataPrev.client.concentration.city !==
					dataCurr.client.concentration.city
			) {
				return true;
			}
			if (
				dataPrev.client.concentration.country !==
					dataCurr.client.concentration.country
			) {
				return true;
			}
			if (
				dataPrev.client.concentration.state !==
					dataCurr.client.concentration.state
			) {
				return true;
			}
		}
		if ( dataPrev.client.count !== dataCurr.client.count ) {
			return true;
		}
		if ( !arrayCompare( dataPrev.client.countries, dataCurr.client.countries ) ) {
			return true;
		}
		if ( !arrayCompare( dataPrev.client.proxies, dataCurr.client.proxies ) ) {
			return true;
		}
		if ( !arrayCompare( dataPrev.client.types, dataCurr.client.types ) ) {
			return true;
		}
	}
	if ( !!dataPrev.location !== !!dataCurr.location ) {
		return true;
	}
	if ( dataPrev.location ) {
		// location exists on both
		if ( dataPrev.location.country !== dataCurr.location.country ) {
			return true;
		}
	}
	if ( !arrayCompare( dataPrev.risks, dataCurr.risks ) ) {
		return true;
	}
	if ( !!dataPrev.tunnels !== !!dataCurr.tunnels ) {
		return true;
	}
	if ( dataPrev.tunnels ) {
		// tunnels exists on both
		if ( !tunnelsArrayCompare( dataPrev.tunnels, dataCurr.tunnels ) ) {
			return true;
		}
	}
	return false;
}

function returnAsEncapsulatedString( array ) {
	return array.map( function ( item ) {
		return sqlString.escape( item );
	} ).join( ',' );
}

function generateInsertActorQueries( actor ) {
	const actorObj = {
		actor_data: {
			ip: actor.ip,
			org: actor.organization,
			client_count: actor.client.count || 0,
			types: actor.client.types ?
				getActorTypes( actor.client.types ) : actorTypes.UNKNOWN,
			conc_city:
			actor.client.concentration && actor.client.concentration.city ? actor.client.concentration.city : '',
			conc_state:
			actor.client.concentration && actor.client.concentration.state ? actor.client.concentration.state : '',
			conc_country:
			actor.client.concentration && actor.client.concentration.country ? actor.client.concentration.country : '',
			countries: actor.client.countries || 0,
			location_country: actor.location.country || '',
			risks: actor.risks ? getActorRisks( actor.risks ) : riskTypes.UNKNOWN
		},
		behaviors: actor.client.behaviors && Array.isArray( actor.client.behaviors ) ?
			getActorBehaviors( actor.client.behaviors ) : [],
		proxies: actor.client.proxies && Array.isArray( actor.client.proxies ) ?
			getActorProxies( actor.client.proxies ) : [],
		tunnels: actor.tunnels && Array.isArray( actor.tunnels ) ?
			getTunnels( actor.tunnels ) : false
	};

	let queries = '';
	queries += `INSERT INTO actor_data (${Object.keys( actorObj.actor_data ).toString()}) VALUES (${returnAsEncapsulatedString( Object.values( actorObj.actor_data ) )});@@@@@`;
	actorObj.behaviors.forEach( function ( behavior ) {
		queries += `INSERT INTO actor_data_behaviors (actor_data_id, behavior_id) VALUES( (SELECT pkid FROM actor_data WHERE ip = '${actorObj.actor_data.ip}'), (SELECT pkid FROM behaviors WHERE behavior = '${behavior}') );@@@@@`;
	} );
	actorObj.proxies.forEach( function ( proxy ) {
		queries += `INSERT INTO actor_data_proxies (actor_data_id, proxy_id) VALUES( (SELECT pkid FROM actor_data WHERE ip = '${actorObj.actor_data.ip}'), (SELECT pkid FROM proxies WHERE proxy = '${proxy}' ) );@@@@@`;
	} );
	if ( actorObj.tunnels ) {
		actorObj.tunnels.forEach( function ( tunnel ) {
			queries += `INSERT INTO actor_data_tunnels (actor_data_id, tunnel_id) VALUES( (SELECT pkid FROM actor_data WHERE ip = '${actorObj.actor_data.ip}'), (SELECT pkid FROM tunnels WHERE operator = '${tunnel.operator}' ) );@@@@@`;
		} );
	}
	return queries;
}

var as_strat = fc.record(
	{
		organization: fc.string(),
		number: fc.nat()
	},
	{ requiredKeys: [] }
);

var concentration_strat = fc.record(
	{
		city: fc.string(),
		country: fc.string(),
		density: fc.nat(),
		geohash: fc.string(),
		skew: fc.float( { min: 0, max: 1 } ),
		state: fc.string()
	},
	{ requiredKeys: [] }
);

var client_strat = fc.record(
	{
		behaviors: fc.array(fc.string()),
		concentration: concentration_strat,
		count: fc.nat(),
		countries: fc.nat(),
		proxies: fc.array(fc.string()),
		spread: fc.nat(),
		types: fc.subarray(['DESKTOP', 'HEADLESS', 'IOT', 'MOBILE'])
	},
	{ requiredKeys: [] }
);

var location_strat = fc.dictionary(fc.constantFrom('city', 'country', 'state'), fc.string());

var tunnel = fc.record(
	{
		type: fc.constantFrom("VPN", "PROXY", "OTHER"),
		operator: fc.string(),
		anonymous: fc.boolean(),
		entries: fc.array(fc.string()),
		exits: fc.array(fc.string())
	},
	{ requiredKeys: [ 'type' ] }
);

var tunnels_strat = fc.array(fc.record(
	{
		type: fc.constantFrom("VPN", "PROXY", "OTHER"),
		operator: fc.string(),
		anonymous: fc.boolean(),
		entries: fc.array(fc.string()),
		exits: fc.array(fc.string())
	},
	{ requiredKeys: [ 'type' ] }
));

var spur = fc.record(
	{
		as: as_strat,
		client: client_strat,
		infrastructure: fc.subarray(['DATACENTER', 'MOBILE', 'SATELLITE', 'IN_FLIGHT_WIFI', 'GOOGLE']),
		location: location_strat,
		organization: fc.string(),
		risks: fc.subarray(['CALLBACK_PROXY', 'GEO_MISMATCH', 'LOGIN_BRUTEFORCE', 'TUNNEL', 'WEB_SCRAPING']),
		services: fc.array(fc.string()),
		tunnels: tunnels_strat
	},
	{ requiredKeys: [] }
);

describe( 'output-sql.js', function () {
	it( 'arrayCompare', function () {
		fc.assert(
			fc.property( fc.array(fc.string()), fc.array(fc.string()), function ( array1, array2 ) {
				// console.log( array1, array2 );
				try {
					arrayCompare( array1, array2 );
					if ( !arrayCompare( array1, array1 ) ) {
						return false;
					}
					if ( !arrayCompare( array2, array2 ) ) {
						return false;
					}
					return true;
				} catch ( error ) {
					console.log( error );
					return false;
				}
			})
		);
	});

	it( 'tunnelsCompare', function () {
		fc.assert(
			fc.property( fc.dictionary(fc.string(), fc.string()), fc.dictionary(fc.string(), fc.string()), function ( array1, array2 ) {
				// console.log( array1, array2 );
				try {
					tunnelsCompare( array1, array2 );
					if ( !tunnelsCompare( array1, array1 ) ) {
						return false;
					}
					if ( !tunnelsCompare( array2, array2 ) ) {
						return false;
					}
					return true;
				} catch ( error ) {
					console.log( error );
					return false;
				}
			})
		);
	});

	it( 'tunnelsCompare with realistic data', function () {
		fc.assert(
			fc.property( tunnel, tunnel, function ( array1, array2 ) {
				// console.log( array1, array2 );
				try {
					tunnelsCompare( array1, array2 );
					if ( !tunnelsCompare( array1, array1 ) ) {
						return false;
					}
					if ( !tunnelsCompare( array2, array2 ) ) {
						return false;
					}
					return true;
				} catch ( error ) {
					console.log( error );
					return false;
				}
			})
		);
	});

	it( 'tunnelsSort', function () {
		fc.assert(
			fc.property( fc.dictionary(fc.string(), fc.string()), fc.dictionary(fc.string(), fc.string()), function ( array1, array2 ) {
				// console.log( array1, array2 );
				try {
					tunnelsSort( array1, array2 );
					if ( tunnelsSort( array1, array1 ) != 0 ) {
						return false;
					}
					if ( tunnelsSort( array2, array2 ) != 0 ) {
						return false;
					}
					return true;
				} catch ( error ) {
					console.log( error );
					return false;
				}
			})
		);
	});

	it( 'tunnelsSort with realistic data', function () {
		fc.assert(
			fc.property( tunnel, tunnel, function ( array1, array2 ) {
				// console.log( array1, array2 );
				try {
					tunnelsSort( array1, array2 );
					if ( tunnelsSort( array1, array1 ) != 0 ) {
						return false;
					}
					if ( tunnelsSort( array2, array2 ) != 0 ) {
						return false;
					}
					return true;
				} catch ( error ) {
					console.log( error );
					return false;
				}
			})
		);
	});

	it( 'tunnelsArrayCompare', function () {
		fc.assert(
			fc.property( fc.array(fc.dictionary(fc.string(), fc.string())), fc.array(fc.dictionary(fc.string(), fc.string())), function ( array1, array2 ) {
				// console.log( array1, array2 );
				try {
					tunnelsArrayCompare( array1, array2 );
					if ( !tunnelsArrayCompare( array1, array1 ) ) {
						return false;
					}
					if ( !tunnelsArrayCompare( array2, array2 ) ) {
						return false;
					}
					return true;
				} catch ( error ) {
					console.log( error );
					return false;
				}
			})
		);
	});

	it( 'tunnelsArrayCompare with realistic data', function () {
		fc.assert(
			fc.property( tunnels_strat, tunnels_strat, function ( array1, array2 ) {
				// console.log( array1, array2 );
				try {
					tunnelsArrayCompare( array1, array2 );
					if ( !tunnelsArrayCompare( array1, array1 ) ) {
						return false;
					}
					if ( !tunnelsArrayCompare( array2, array2 ) ) {
						return false;
					}
					return true;
				} catch ( error ) {
					console.log( error );
					return false;
				}
			})
		);
	});

	it( 'checkIfChanged same', function () {
		fc.assert(
			fc.property( spur, function ( array ) {
				// console.log( array1, array2 );
				try {
					return !checkIfChanged( array, array );
				} catch ( error ) {
					console.log( error );
					return false;
				}
			})
		);
	});

	it( 'checkIfChanged different', function () {
		fc.assert(
			fc.property( spur, spur, function ( array1, array2 ) {
				// console.log( array1, array2 );
				try {
					checkIfChanged( array1, array2 );
					return true;
				} catch ( error ) {
					console.log( error );
					return false;
				}
			})
		);
	});

	it( 'generateInsertActorQueries', function () {
		fc.assert(
			fc.property( spur, function ( array ) {
				// console.log( array1, array2 );
				try {
					generateInsertActorQueries( array );
					return true;
				} catch ( error ) {
					console.log( array );
					console.log( error );
					return false;
				}
			})
		);
	});
});
