SELECT ad.ip, ad.org, ad.client_count, ad.types, ad.conc_geohash, ad.conc_city, ad.conc_state,
       ad.conc_country, ad.conc_skew, ad.conc_density, ad.countries, ad.location_country, ad.risks,
       GROUP_CONCAT(DISTINCT b.behavior) AS behaviors, GROUP_CONCAT(DISTINCT p.proxy) AS proxies,
       GROUP_CONCAT(DISTINCT CONCAT(t.operator, " ", t.type, " ", t.anonymous)) AS tunnels
FROM actor_data AS ad
# Behaviours
LEFT JOIN actor_data_behaviors AS adb ON ad.pkid=adb.actor_data_id
LEFT JOIN behaviors AS b ON adb.behavior_id=b.pkid
# Proxies
LEFT JOIN actor_data_proxies AS adp ON ad.pkid=adp.actor_data_id
LEFT JOIN proxies AS p ON adp.proxy_id=p.pkid
# Tunnels
LEFT JOIN actor_data_tunnels AS adt ON ad.pkid=adt.actor_data_id
LEFT JOIN tunnels AS t ON adt.tunnel_id=t.pkid
GROUP BY ad.pkid ORDER BY ad.ip;

SELECT ip, org, client_count, types, conc_geohash, conc_city, conc_state, conc_country, conc_skew, conc_density, countries, location_country, risks FROM actor_data ORDER BY ip;
SELECT ad.ip, COUNT(adb.actor_data_id) FROM actor_data_behaviors AS adb INNER JOIN actor_data AS ad ON actor_data_id=pkid GROUP BY ip ORDER BY ip;
SELECT ad.ip, COUNT(adp.actor_data_id) FROM actor_data_proxies AS adp INNER JOIN actor_data AS ad ON actor_data_id=pkid GROUP BY ip ORDER BY ip;
SELECT ad.ip, COUNT(adt.actor_data_id) FROM actor_data_tunnels AS adt INNER JOIN actor_data AS ad ON actor_data_id=pkid GROUP BY ip ORDER BY ip;
SELECT behavior FROM behaviors ORDER BY behavior;
SELECT proxy FROM proxies ORDER BY proxy;
SELECT operator, type, anonymous FROM tunnels ORDER BY operator;
