# Copyright (C) 2023 Dominic Walden

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
# http://www.gnu.org/copyleft/gpl.html

# Example usage:
# python3 ipoid_api_accuracy.py -u http://localhost:6927 -f /path/to/spur.feed.json


import argparse
import requests

from pyspark.sql import SparkSession
from pyspark.sql.functions import array_contains

args = argparse.ArgumentParser()
args.add_argument('-f', '--file', required=True)
args.add_argument('-u', '--url', required=True)
options = args.parse_args()

print("Making the spark session...")
# spark = SparkSession.builder.master("spark://192.168.10.117:7077").getOrCreate()
spark = SparkSession.builder.config("spark.executor.memory", "12g").config("spark.driver.memory", "12g").config("spark.memory.offHeap.enabled", "true").config("spark.memory.offHeap.size", "12g").getOrCreate()
print("Reading the json file...")
df = spark.read.json(options.file)

print("Making the request to /proxies...")
proxies = requests.get("{}/feed/v1/proxies".format(options.url)).json()

for proxy_name, ips in proxies.items():
    print("==== {} ====".format(proxy_name))
    expected = df.filter(array_contains(df.client.proxies, proxy_name))

    print("== Compare file with response from /proxies ==")
    match = expected.count() == len(ips)
    print("{}{}{}".format('\033[92m' if match else '\033[91m', match, '\033[0m'))
    # Does not catch the case when the counts might match coincidentally
    if not match:
        actual = spark.createDataFrame(data=[[ip] for ip in ips], schema=['ip'])
        no_match = expected.join(actual, expected.ip == actual.ip, "anti")
        no_match_count = no_match.count()
        if no_match_count > 0:
            print(no_match_count)
            no_match.show()
        no_match = actual.join(expected, actual.ip == expected.ip, "anti")
        no_match_count = no_match.count()
        if no_match_count > 0:
            print(no_match_count)
            no_match.show()

    print("== Compare file with response from /proxy/{} ==".format(proxy_name))
    proxy = requests.get("{}/feed/v1/proxy/{}".format(options.url, proxy_name)).json()
    match = expected.count() == len(proxy)
    print("{}{}{}".format('\033[92m' if match else '\033[91m', match, '\033[0m'))
    # Does not catch the case when the counts might match coincidentally
    if not match:
        actual = spark.createDataFrame(data=[[ip] for ip in proxy], schema=['ip'])
        no_match = expected.join(actual, expected.ip == actual.ip, "anti")
        no_match_count = no_match.count()
        if no_match_count > 0:
            print(no_match_count)
            no_match.show()
        no_match = actual.join(expected, actual.ip == expected.ip, "anti")
        no_match_count = no_match.count()
        if no_match_count > 0:
            print(no_match_count)
            no_match.show()

print("Making the request to /vpns...")
vpns = requests.get("{}/feed/v1/vpns".format(options.url)).json()

for vpn_name, ips in vpns.items():
    print("==== {} ====".format(vpn_name))
    expected = df.filter("exists(tunnels, x -> x.operator == '{}')".format(vpn_name))

    print("== Compare file with response from /vpns ==")
    match = expected.count() == len(ips)
    print("{}{}{}".format('\033[92m' if match else '\033[91m', match, '\033[0m'))
    # Does not catch the case when the counts might match coincidentally
    if not match:
        actual = spark.createDataFrame(data=[[ip] for ip in ips], schema=['ip'])
        no_match = expected.join(actual, expected.ip == actual.ip, "anti")
        no_match_count = no_match.count()
        if no_match_count > 0:
            print(no_match_count)
            no_match.show()
        no_match = actual.join(expected, actual.ip == expected.ip, "anti")
        no_match_count = no_match.count()
        if no_match_count > 0:
            print(no_match_count)
            no_match.show()

    print("== Compare file with response from /vpn/{} ==".format(vpn_name))
    vpn = requests.get("{}/feed/v1/vpn/{}".format(options.url, vpn_name)).json()
    match = expected.count() == len(vpn)
    print("{}{}{}".format('\033[92m' if match else '\033[91m', match, '\033[0m'))
    # Does not catch the case when the counts might match coincidentally
    if not match:
        actual = spark.createDataFrame(data=[[ip] for ip in vpn], schema=['ip'])
        no_match = expected.join(actual, expected.ip == actual.ip, "anti")
        no_match_count = no_match.count()
        if no_match_count > 0:
            print(no_match_count)
            no_match.show()
        no_match = actual.join(expected, actual.ip == expected.ip, "anti")
        no_match_count = no_match.count()
        if no_match_count > 0:
            print(no_match_count)
            no_match.show()
