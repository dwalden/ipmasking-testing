# Copyright (C) 2023 Dominic Walden

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
# http://www.gnu.org/copyleft/gpl.html

# Usage:
# python3 spur_random_data_valid_1.py -n 1000

import json
import string
import argparse

from hypothesis import given, settings, Phase
import hypothesis.strategies as st


args = argparse.ArgumentParser()
args.add_argument('-n', '--num', type=int, default=1000,
                  help="Number of test entries to generate.")
args.add_argument('-f', '--filter', action='store_true', default=False,
                  help="Should the output filter out duplicate IPs?")
options = args.parse_args()


def text_list(max_size):
    return st.lists(st.text(alphabet=string.ascii_letters, min_size=1, max_size=max_size), unique=True)


text_atom = st.text(alphabet=string.ascii_letters, min_size=1, max_size=32)
integers_atom = st.integers(min_value=0, max_value=999999)
booleans_atom = st.booleans()

as_strat = st.fixed_dictionaries({},
                                 optional={'organization': text_atom,
                                           'number': integers_atom})

concentration_strat = st.fixed_dictionaries({},
                                            optional={'city': text_atom,
                                                      'country': text_atom,
                                                      'density': integers_atom,
                                                      'geohash': text_atom,
                                                      'skew': st.floats(0, 1),
                                                      'state': text_atom})

client_strat = st.fixed_dictionaries({},
                                     optional={'behaviors': text_list(64),
                                               'concentration': concentration_strat,
                                               'count': integers_atom,
                                               'countries': integers_atom,
                                               'proxies': text_list(128),
                                               'spread': integers_atom,
                                               'types': st.lists(st.sampled_from(['DESKTOP', 'HEADLESS', 'IOT', 'MOBILE']), min_size=0)})

location_strat = st.dictionaries(st.sampled_from(['city', 'country', 'state']), text_atom)

tunnels_strat = st.lists(st.fixed_dictionaries({'type': st.sampled_from(["VPN", "PROXY"])},
                                               optional={'operator': text_atom,
                                                         'anonymous': booleans_atom,
                                                         'entries': text_list(10),
                                                         'exits': text_list(10)}),
                         min_size=0, unique_by=(lambda x: None if 'operator' not in x else x['operator'], lambda x: x['type']))

strat = st.fixed_dictionaries({'ip': st.ip_addresses()},
                              optional={'as': as_strat,
                                        'client': client_strat,
                                        'infrastructure': st.lists(st.sampled_from(['DATACENTER', 'MOBILE', 'SATELLITE', 'IN_FLIGHT_WIFI', 'GOOGLE'])),
                                        'location': location_strat,
                                        'organization': text_atom,
                                        'risks': st.lists(st.sampled_from(['CALLBACK_PROXY', 'GEO_MISMATCH', 'LOGIN_BRUTEFORCE', 'TUNNEL', 'WEB_SCRAPING'])),
                                        'services': st.lists(st.text()),
                                        'tunnels': tunnels_strat})

ips = set()


@given(strat)
@settings(max_examples=options.num, deadline=None, phases=[Phase.generate])
def generate_test_data(d):
    d['ip'] = str(d['ip'])
    if options.filter:
        if str(d['ip']) not in ips:
            ips.add(str(d['ip']))
            print(json.dumps(d, ensure_ascii=True))
    else:
        print(json.dumps(d, ensure_ascii=True))


if __name__ == "__main__":
    generate_test_data()
