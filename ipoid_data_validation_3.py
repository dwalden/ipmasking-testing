# Copyright (C) 2023 Dominic Walden

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
# http://www.gnu.org/copyleft/gpl.html

# Example usage:
# 1. Find out the IP of the ipoid database by running:
#    docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' ipoid_db_1
# 2. python3 ipoid_data_validation_3.py -f /path/to/spur.feed.json -s <ip of ipoid database> -u ipoid_ro -p password3

import argparse
import json
import pymysql
import traceback
from spur import Spur


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def match(left, right, label):
    if left != right:
        print("{} don't match: {} {}".format(label, left, right))


args = argparse.ArgumentParser()
args.add_argument('-f', '--file', required=True)
args.add_argument('-s', '--server', default="172.17.0.1")
args.add_argument('-P', '--port', type=int, default=3306)
args.add_argument('-u', '--user', default="root")
args.add_argument('-p', '--password', default="password1")
args.add_argument('-d', '--database', default="ipoid")
options = args.parse_args()

query = """
SELECT ad.ip, ad.client_count, ad.types, ad.risks,
       GROUP_CONCAT(DISTINCT IF(b.pkid IS NOT NULL AND b.behavior IS NOT NULL, b.behavior, IF(b.pkid IS NOT NULL AND b.behavior IS NULL, "NULL", "None"))) AS behaviors,
       GROUP_CONCAT(DISTINCT IF(p.pkid IS NOT NULL AND p.proxy IS NOT NULL, p.proxy, IF(p.pkid IS NOT NULL AND p.proxy IS NULL, "NULL", "None"))) AS proxies,
       GROUP_CONCAT(DISTINCT CONCAT(t.operator, " ", IF(t.type IS NOT NULL, t.type, "None"), " ", IF(t.anonymous IS NOT NULL, t.anonymous, "None"))) AS tunnels
FROM actor_data AS ad
# Behaviours
LEFT JOIN actor_data_behaviors AS adb ON ad.pkid=adb.actor_data_id
LEFT JOIN behaviors AS b ON adb.behavior_id=b.pkid
# Proxies
LEFT JOIN actor_data_proxies AS adp ON ad.pkid=adp.actor_data_id
LEFT JOIN proxies AS p ON adp.proxy_id=p.pkid
# Tunnels
LEFT JOIN actor_data_tunnels AS adt ON ad.pkid=adt.actor_data_id
LEFT JOIN tunnels AS t ON adt.tunnel_id=t.pkid
WHERE ad.ip="{}"
GROUP BY ad.pkid;
"""

connection = pymysql.connect(host=options.server,
                             port=options.port,
                             user=options.user,
                             password=options.password,
                             database=options.database,
                             cursorclass=pymysql.cursors.DictCursor)

tunnel_id = {
    'UNKNOWN': "0",
    'VPN': "1",
    'PROXY': "2"
}

actor_types_id = {
    'UNKNOWN': 0b00001,
    'DESKTOP': 0b00010,
    'HEADLESS': 0b00100,
    'IOT': 0b01000,
    'MOBILE': 0b10000
}

risk_types_id = {
    'UNKNOWN': 0b000001,
    'CALLBACK_PROXY': 0b000010,
    'GEO_MISMATCH': 0b000100,
    'LOGIN_BRUTEFORCE': 0b001000,
    'TUNNEL': 0b010000,
    'WEB_SCRAPING': 0b100000
}

# Initialise the "expected" values. Use defaults if, for example, values don't exist.
ips_not_in_db = []
end_results = {}
with open(options.file, "r") as fp:
    for line in fp:
        try:
            raw_json = json.loads(line)
            spur = Spur(raw_json)
            json_comp = {
                "ip": str.encode(spur.ip) if type(spur.ip) is str else spur.ip,
                "client_count": spur.client_count,
                "types": actor_types_id["UNKNOWN"] if len(spur.types) == 0 else sum([actor_types_id[i] for i in spur.types]),
                "risks": risk_types_id["UNKNOWN"] if len(spur.risks) == 0 else sum([risk_types_id[i] for i in spur.risks]),
                "behaviors": str.encode("None") if len(spur.behaviors) == 0 else str.encode(",".join(spur.behaviors), errors='surrogateescape'),
                "proxies": str.encode("None") if len(spur.proxies) == 0 else str.encode(",".join(spur.proxies), errors='surrogateescape'),
                # str(tunnel['anonymous'])
                "tunnels": None if len(spur.tunnels) == 0 else str.encode(",".join([" ".join([str(tunnel['operator']), tunnel_id[str(tunnel['type'])], "None"]) for tunnel in spur.tunnels]), errors='surrogateescape')
            }
            cursor = connection.cursor()
            cursor.execute(query.format(spur.ip))
            ip = cursor.fetchone()
            if not ip:
                ips_not_in_db.append(spur.ip)
            elif ip and ip != json_comp:
                print(ip)
                print("{}=== {} does not match ==={}".format(bcolors.WARNING, ip['ip'].decode(), bcolors.ENDC))
                print("       Raw JSON: {}".format(raw_json))
                print("Normalised JSON: {}".format(json_comp))
                print("            SQL: {}".format(ip))
                for var, value in ip.items():
                    if value != json_comp[var]:
                        print("{}{} {} don't match: {} {}{}".format(bcolors.FAIL, ip['ip'].decode(), var, value, json_comp[var], bcolors.ENDC))
                        if var not in end_results:
                            end_results[var] = [ip['ip'].decode()]
                        else:
                            end_results[var].append(ip['ip'].decode())
        except Exception as err:
            print("{}Exception parsing {}{}".format(bcolors.HEADER, line, bcolors.ENDC))
            print(err)
            traceback.print_exc()

if len(end_results) > 0:
    print("{}==== Variables not matching ===={}".format(bcolors.WARNING, bcolors.ENDC))
    for var, ips in end_results.items():
        print("{}: {}".format(var, len(ips)))

if len(ips_not_in_db) > 0:
    print("{}==== IPs in JSON that are not in database ===={}".format(bcolors.WARNING, bcolors.ENDC))
    print(ips_not_in_db)
    print("Total: {}".format(len(ips_not_in_db)))

# Checking for duplicate rows
queries = {
    "actor_data": "SELECT * FROM (SELECT ip, COUNT(*) AS duplicates FROM actor_data GROUP BY ip) AS foo WHERE duplicates > 1;",
    "actor_data_tunnels": "SELECT * FROM (SELECT *, COUNT(*) AS duplicates FROM actor_data_tunnels GROUP BY actor_data_id,tunnel_id) AS foo WHERE duplicates > 1;",
    "actor_data_behaviors": "SELECT * FROM (SELECT *, COUNT(*) AS duplicates FROM actor_data_behaviors GROUP BY actor_data_id,behavior_id) AS foo WHERE duplicates > 1;",
    "actor_data_proxies": "SELECT * FROM (SELECT *, COUNT(*) AS duplicates FROM actor_data_proxies GROUP BY actor_data_id,proxy_id) AS foo WHERE duplicates > 1;",
    "behaviors": "SELECT * FROM (SELECT *, COUNT(*) AS duplicates FROM behaviors GROUP BY behavior) AS foo WHERE duplicates > 1;",
    "proxy": "SELECT * FROM (SELECT *, COUNT(*) AS duplicates FROM proxies GROUP BY proxy) AS foo WHERE duplicates > 1;",
    "tunnels": "SELECT * FROM (SELECT *, COUNT(*) AS duplicates FROM tunnels GROUP BY operator) AS foo WHERE duplicates > 1;"
}

for table, query in queries.items():
    cursor.execute(query)
    result = cursor.fetchall()
    if len(result) > 0:
        print("{}=== Duplicate entries in {} ==={}".format(bcolors.WARNING, table, bcolors.ENDC))
        print(result)

query = """
SELECT ip, risks, types
FROM actor_data
WHERE NOT(risks >= 1 AND risks <= 64)
OR NOT(types >= 1 AND types <= 32);
"""
cursor.execute(query)
result = cursor.fetchall()
if len(result) > 0:
    # invalid_rows = []
    # for row in result:
    #     invalid = False
    #     for typei in row['types'].split(","):
    #         if typei not in ['UNKNOWN', 'DESKTOP', 'HEADLESS', 'IOT', 'MOBILE']:
    #             invalid = True
    #     for risk in row['risks'].split(","):
    #         if risk not in ['UNKNOWN', 'CALLBACK_PROXY', 'GEO_MISMATCH', 'LOGIN_BRUTEFORCE', 'TUNNEL', 'WEB_SCRAPING']:
    #             invalid = True
    #     if invalid:
    #         invalid_rows.append(row)
    # if len(invalid_rows) > 0:
    print("{}=== Invalid risks or types ==={}".format(bcolors.WARNING, bcolors.ENDC))
    print(result)

# Checking for "dangling" entries
queries = {
    "actor_data_tunnels": "SELECT * FROM actor_data_tunnels LEFT JOIN actor_data ON actor_data_id=pkid WHERE pkid IS NULL;",
    "actor_data_behaviors": "SELECT * FROM actor_data_behaviors LEFT JOIN actor_data ON actor_data_id=pkid WHERE pkid IS NULL;",
    "actor_data_proxies": "SELECT * FROM actor_data_proxies LEFT JOIN actor_data ON actor_data_id=pkid WHERE pkid IS NULL;",
    "behaviors": "SELECT * FROM behaviors LEFT JOIN actor_data_behaviors ON pkid=behavior_id WHERE actor_data_id IS NULL;",
    "proxy": "SELECT * FROM proxies LEFT JOIN actor_data_proxies ON pkid=proxy_id WHERE actor_data_id IS NULL;",
    "tunnels": "SELECT * FROM tunnels LEFT JOIN actor_data_tunnels ON pkid=tunnel_id WHERE actor_data_id IS NULL;"
}

for table, query in queries.items():
    cursor.execute(query)
    result = cursor.fetchall()
    if len(result) > 0:
        print("{}=== Dangling entries in {} ==={}".format(bcolors.WARNING, table, bcolors.ENDC))
        print(result)

cursor.close()
connection.close()
